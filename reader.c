#include <stdio.h>
#include <stdlib.h>
#include "reader.h"

FILE* openFile(char* filename, char* type)
{
  FILE* inputFile = fopen(filename, type); 
  if (inputFile == NULL)
  {
    printf("Mauvais fichier (%s)\n", filename);
    exit(1);
  }

  return inputFile;
}

void closeFile(FILE* file)
{
  int close;
  close = fclose(file);

  if (close == EOF)
  {
    printf("Could not close file\n");
    exit(1);
  }
}
