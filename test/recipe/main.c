#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "category.c"
#include "recipe.c"

int main(int argc, char *argv[])
{
  Recipe_t recipe6 = newRecipe("ZBA");  
  Recipe_t recipe = newRecipe("AAA");  
  Recipe_t recipe1 = newRecipe("BBB");  
  Recipe_t recipe2 = newRecipe("CCC");  
  Recipe_t recipe3 = newRecipe("HHH");  
  Recipe_t recipe4 = newRecipe("AAB");  
  Recipe_t recipe5 = newRecipe("BBA");  
  
  printf("Test SUCCESS (AAA = %s)\n", recipe->description );
  Category_t cat = newCategory("Ma catégorie", NULL);

  addRecipe(cat, recipe);
  addRecipe(cat, recipe1);
  addRecipe(cat, recipe2);
  addRecipe(cat, recipe3);
  addRecipe(cat, recipe4);
  addRecipe(cat, recipe5);
  addRecipe(cat, recipe6);

  recipeToString(cat->recipeHead);
  return 0;
}
