#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "category.h"
#include "recipe.h"

Category_t newCategory(char* description, char* recipe)
{
  Category_t category;
  category = malloc(sizeof(CategoryIntern_t));
  if(recipe != NULL)
  {
    category->recipeHead = newRecipe(recipe);
  }

  strcpy(category->description, description);

  return category;
}

void addRecipe(Category_t category, Recipe_t recipe)
{
  Recipe_t current = category->recipeHead;  
  
  if(category->recipeHead == NULL)
  {
    category->recipeHead = recipe;
    return;
  }
  
  if(compareDescription(recipe->description, current->description) < 0)
  {
    recipe->nextRecipe = current; 
    category->recipeHead = recipe;
    return;
  }
  addRecipeInChain(current, recipe);
}

void addRecipeInChain(Recipe_t current, Recipe_t recipe)
{
  while(current->nextRecipe != NULL)
  {
    if(strcmp(current->description, recipe->description) == 0)
    {
      //2 recipes with the same name
      //FREE
      return;
    }
    if(compareDescription(recipe->description, current->nextRecipe->description) < 0)
    {
      recipe->nextRecipe = current->nextRecipe;
      current->nextRecipe = recipe;
      return;
    }
    current = current->nextRecipe;
  }
  current->nextRecipe = recipe;
}

Category_t addCategory(Category_t head, Category_t newCat)
{
  //newCat is the new head
  if(head == NULL)
  {
    return newCat;
  }
 
  //Insert new cat before
  if(compareDescription(newCat->description, head->description) < 0)
  {
    newCat->nextCategory = head; 
    return newCat;
  }

  //Run down chain list for the right spot, return head
  addCategoryInChain(head, newCat);
  return head;
}

void addCategoryInChain(Category_t head, Category_t newCat)
{
  Category_t current = head;
  while(current->nextCategory != NULL)
  {
    if(strcmp(current->description, newCat->description) == 0)
    {
      //Category already exist
      addRecipe(current, newCat->recipeHead);
      //FREE
      return; 
    } 
    else if(compareDescription(newCat->description, current->nextCategory->description) < 0)
    {
      newCat->nextCategory = current->nextCategory;
      current->nextCategory = newCat;
      return;
    }
    current = current->nextCategory;
  }
  if(strcmp(current->description, newCat->description) == 0)
  {
    //Category already exist
    addRecipe(current, newCat->recipeHead);
    //FREE
    return; 
  }
  current->nextCategory = newCat;
}

void categoryToString(Category_t head)
{
  Category_t current = head;
  int  cnt = 1;

  if(head == NULL)
  {
    printf("Catégorie vide, rien à imprimer\n");
    exit(1);
  }

  while(current->nextCategory != NULL)
  {
    printf("%d - %s\n", cnt++, current->description);
    recipeToString(current->recipeHead);
    current = current->nextCategory;
  }

  printf("%d - %s\n", cnt++, current->description);
  recipeToString(current->recipeHead);
}

int compareDescription(char* string1, char* string2)
{
  int idx = 0;
  while (string1[idx] != '\0' && string2[idx] != '\0')
  {
    if(string1[idx] > string2[idx])
    {
      return 1;
    }
    else if(string1[idx] < string2[idx])
    {
      return -1;
    }
    idx++;
  }
  return strcmp(string1, string2);
}

void removeCategory(Category_t category)
{
  //FREE recipe first
  free(category);
}
