#ifndef REP_H
#define REP_H

typedef struct RecipeIntern_t
{
  char description[120]; 
  struct RecipeIntern_t *nextRecipe;
} RecipeIntern_t;

typedef RecipeIntern_t* Recipe_t;

/**
 * Create new recipe
 */
Recipe_t newRecipe(char* description);

/**
 * Print each member of the array list
 */
void recipeToString(Recipe_t head);
/**
 * Free memory
 */
void removeRecipe(Recipe_t recipe);

#endif
