#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "recipe.h"

Recipe_t newRecipe(char* description)
{
  Recipe_t recipe;
  recipe = malloc(sizeof(RecipeIntern_t));
  
  if(description != NULL)
  {
    strcpy(recipe->description, description);
  }

  return recipe;
}

void recipeToString(Recipe_t head)
{
  Recipe_t current = head;
  int cnt = 1;
  while(current->nextRecipe != NULL)
  {
    printf("   %d - %s\n", cnt++, current->description);
    current = current->nextRecipe;
  }

  printf("   %d - %s\n", cnt++, current->description);
}

void removeRecipe(Recipe_t recipe)
{
  //FREE recipe first
  free(recipe);
}
