#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <ctype.h>
#include <string.h>
#include "search.h"
#include "category.h"
#include "recipe.h"

const int maxLineSize = 120;
const int minSizeOfchunk = 10; //for dynamic string size allocation

void launchSearch(Category_t categoryHead)
{
  char* searchInput;
  do
  {
    searchInput = promptSearch();
    if(validateSearchInput(searchInput) == 1)
    {
      search(categoryHead, searchInput);
    }
    else
    {
      printf("Recherche invalide.\n");
    }
    free(searchInput);
  }while(1);
}

int validateSearchInput(char* searchInput)
{
  int idx = 0;
  
  while(searchInput[idx] != '\0')
  {
    if(!isspace(searchInput[idx]) && searchInput[idx] != '\n')
    {
      return 1;
    }
    idx++;
  }

  return 0;
}

char* promptSearch()
{
  char* searchInput;
  printf("Entrez votre critère de recherche : ");
  searchInput = getStdin();
  return searchInput;
}

void search(Category_t head, char* searchInput)
{
   
  int inputStatus;  

  searchInput = trim(searchInput);
  inputStatus = checkSearchInput(searchInput);
  if(inputStatus == -1)
  {
    printf("Recherche invalide.\n");
  }
  else
  {
    postSearchValidation(head, searchInput, inputStatus);  
  }

  //FREE category
  //FREE recipe
}

void postSearchValidation(Category_t head, char* searchInput, int searchType)
{
  char *searchCategory;  
  char *searchRecipe; 
  Category_t categoryResult;

  searchCategory = findCategory(searchInput); 
  if(searchType == 1)
  {
    searchRecipe = findRecipe(searchInput); 
    recipeSearchResult(head, searchCategory, searchRecipe);
  }
  else
  {
    categoryResult = categorySearchResult(head, searchCategory);
    if(categoryResult != NULL)
    {
      recipeToString(categoryResult->recipeHead);
    }
    else
    {
      printf("Catégorie inexistante.\n");
    }
  }
}

Category_t categorySearchResult(Category_t head, char* searchCategory)
{
  Category_t current = head;
  while(current->nextCategory != NULL)
  {
    if(strcmp(current->description, searchCategory) == 0)
    {
      return current;
    }
    current = current->nextCategory;
  }
  if(strcmp(current->description, searchCategory) == 0)
  {
    return current;
  }
  
  return NULL;
}

void recipeSearchResult(Category_t head, char* category, char* recipe)
{
  Category_t cat = categorySearchResult(head, category); 

  if(cat == NULL)
  {
    printf("Catégorie inexistante.\n");
  }
  else
  {
    lookForRecipe(cat, category, recipe);
  }
}

void lookForRecipe(Category_t catStruct, char* category, char* recipe)
{
  Recipe_t recipeStruct;
  int hasResult = 0;

  recipeStruct = catStruct->recipeHead; 

  while(recipeStruct->nextRecipe != NULL)
  {
    if(strstr(recipeStruct->description, recipe) != NULL)
    {
      hasResult = 1;
      printf("%s\n", recipeStruct->description );
    }
    recipeStruct = recipeStruct->nextRecipe;
  }

  if(strstr(recipeStruct->description, recipe) != NULL)
  {
    hasResult = 1;
    printf("%s\n", recipeStruct->description );
  }

  if(hasResult == 0)
  {
    printf("Aucun résultat trouvé.\n");
  }
}

int checkSearchInput(char* searchInput)
{
  int idx = 0;

  while(searchInput[idx] != ' ' && searchInput[idx] != '\0')
    idx++;
  
  //Validate first word
  if(searchInput[idx] == '\0')
    return 0;

  while(searchInput[idx] == ' ')
    idx++;

  while(searchInput[idx] != ' ' && searchInput[idx] != '\0')
    idx++;

  //Validate if more than one word for recipe, wrong
  if(searchInput[idx] != '\0')
    return -1;

  //Both category and recipe
  return 1;
}

char* findCategory(char* searchInput)
{
  int end = 0;
  int idx = 0;
  char* category;
  
  while(!isspace(searchInput[end]) && searchInput[end] != '\0')
    end++;

  category = malloc(end);
  while(idx != end)
  {
    category[idx] = tolower(searchInput[idx]);
    idx++;
  }
    
  category[0] = toupper(category[0]);
  return category;
}

char* findRecipe(char* searchInput)
{
  int start = 0, end = 0, idx = 0;
  char* recipe;

  while(!isspace(searchInput[start]) && searchInput[start] != '\0') 
    start++;

  while(isspace(searchInput[start]) && searchInput[start] != '\0')
    start++;

  end = start + 1;

  while(!isspace(searchInput[end]) && searchInput[end] != '\0')
    end++;
  
<<<<<<< HEAD
  printf("End : %d\n", end );

=======
>>>>>>> a81f6d0436ec594d88b6733f37d7b30227dbaf2d
  recipe = malloc(end - start);
  while(start != end)
    recipe[idx++] = tolower(searchInput[start++]);
    
  recipe[0] = toupper(recipe[0]);
  return recipe;
}

char* getStdin()
{
  char* line = NULL;
  char* temp = NULL;
  int size = 0, idx = 0;
  int c = EOF;

  while(c)
  {
    c = getc(stdin);
    if(c == EOF || c == '\n')
    {
      c = 0;
    }

    if(size <= idx)
    {
      size += minSizeOfchunk;
      temp = realloc(line, size);
      assert(temp != NULL);
      line = temp;
    }
    line[idx++] = c;
  }
  return line;
}

char* trim(char* str)
{
  char *end;

  while(isspace(*str)) str++;

  if(*str == 0) 
    return str;

  end = str + strlen(str) - 1;
  while(end > str && isspace(*end)) end--;

  *(end+1) = 0;

  return str;
}
