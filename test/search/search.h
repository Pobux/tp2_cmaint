#ifndef SEARCH_H
#define SEARCH_H
#include "category.h"

void launchSearch(Category_t categoryHead);

char* promptSearch();

void search(Category_t head, char* searchInput);

char* getStdin();

char* findCategory(char* searchInput);

char* findRecipe(char* searchInput);

int validateSearchInput(char* searchInput);

void lookForRecipe(Category_t catStruct, char* category, char* recipe);

void recipeSearchResult(Category_t head, char* category, char* recipe);

Category_t categorySearchResult(Category_t head, char* category);

void postSearchValidation(Category_t head, char* searchInput, int searchType);

/**
 * 3 states
 * -1, input is wrong
 *  0, there's only a category
 *  1, there's a category and a recipe
 */
int checkSearchInput(char* searchInput);

/**
 * Remove whitespace before and after string
 */
char* trim(char* str);

#endif
