#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "search.c"
#include "category.c"
#include "recipe.c"

int main(int argc, char *argv[])
{
  Category_t head = NULL;
  Category_t cat1 = newCategory("Allo", "Poulet");
  Category_t cat2 = newCategory("Bonjour", "Poulet");
  Category_t cat3 = newCategory("Salut", "Poulet");
  Category_t cat4 = newCategory("Allo", "Boeu");
  /* Category_t cat5 = newCategory("Allo", "crevette"); */

  head = addCategory(head, cat1);
  head = addCategory(head, cat2);
  head = addCategory(head, cat3);
  head = addCategory(head, cat4);
  /* head = addCategory(head, cat5); */

  //Recipe_t recipe6 = newRecipe("ZBA");  
  //Recipe_t recipe = newRecipe("AAA");  
  //Recipe_t recipe1 = newRecipe("BBB");  
  //Recipe_t recipe2 = newRecipe("CCC");  
  //Recipe_t recipe3 = newRecipe("HHH");  
  //Recipe_t recipe4 = newRecipe("AAB");  
  //Recipe_t recipe5 = newRecipe("BBA");  

  //addRecipe(cat1, recipe);
  //addRecipe(cat1, recipe1);
  //addRecipe(cat1, recipe2);
  //addRecipe(cat2, recipe3);
  //addRecipe(cat2, recipe4);
  //addRecipe(cat3, recipe5);
  //addRecipe(cat4, recipe6);
  
  launchSearch(head);  
  return 0;
}
