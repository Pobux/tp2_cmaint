#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "category.c"
#include "recipe.c"

int main(int argc, char *argv[])
{
  Category_t head = NULL;

  Category_t cat7 = newCategory("ACA", "hooo");
  Category_t cat2 = newCategory("ZZZ", "aaa");
  Category_t cat3 = newCategory("ZZZ", "lol");
  Category_t cat11 = newCategory("ZZZ", "bbb");
  Category_t cat10 = newCategory("ACA", "hooo"); // double recipe
  Category_t cat4 = newCategory("CAA", "lol");
  Category_t cat5 = newCategory("ACA", "lol");
  Category_t cat6 = newCategory("ACA", "huuu");
  Category_t cat8 = newCategory("ACAA", "lol");
  Category_t cat9 = newCategory("ZZZ", "bbb"); // double recipe
  
  head = addCategory(head, cat2);
  head = addCategory(head, cat4);
  head = addCategory(head, cat3);
  head = addCategory(head, cat5);
  head = addCategory(head, cat6);
  head = addCategory(head, cat7);
  head = addCategory(head, cat8);
  head = addCategory(head, cat9);
  head = addCategory(head, cat10);
  head = addCategory(head, cat11);
  
  categoryToString(head);
  return 0;
}
