#ifndef REP_H
#define REP_H

typedef struct RecipeIntern_t
{
  char description[120]; 
  struct RecipeIntern_t *nextRecipe;
} RecipeIntern_t;

typedef RecipeIntern_t* Recipe_t;

/**
 * Create new recipe
 */
Recipe_t newRecipe(char* description);

/**
 * Evaluate head
 */
Recipe_t repEvaluateHead(Recipe_t head, Recipe_t newCat);

/**
 * Define after which position new recipe has to be inserted 
 */
Recipe_t repAddAfter(Recipe_t head, Recipe_t newCat);

/**
 * Do the actual insertion by swapping adresses
 */
Recipe_t repDoInsert(Recipe_t after, Recipe_t newCat);

/**
 * Check if a recipe exist
 */
Recipe_t existRecipe(Recipe_t head, Recipe_t newCat);

/**
 * Print each member of the array list
 */
void recipeToString(Recipe_t head);
/**
 * Free memory
 */
void removeRecipe(Recipe_t recipe);

#endif
