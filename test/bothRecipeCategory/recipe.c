#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "recipe.h"

Recipe_t newRecipe(char* description)
{
  Recipe_t recipe;
  recipe = malloc(sizeof(RecipeIntern_t));
  
  if(description != NULL)
  {
    strcpy(recipe->description, description);
  }

  return recipe;
}

Recipe_t repEvaluateHead(Recipe_t head, Recipe_t newRep)
{
  if(head == NULL)
  {
    head = newRep; 
    return head;
  }
  if(strcmp(newRep->description, head->description) < 0)
  {
    newRep->nextRecipe = head;
    return newRep;
  }
  else
  {
    return head;
  }
}

Recipe_t repAddRecipe(Recipe_t head, Recipe_t newRep)
{
  Recipe_t recipe = existRecipe(head, newRep);
  Recipe_t addAfterRep;
  
  if(recipe)
  {
    return recipe; 
  }

  addAfterRep = repAddAfter(head, newRep); 
  return repDoInsert(addAfterRep, newRep);
}

Recipe_t repDoInsert(Recipe_t after, Recipe_t newRep)
{
  newRep->nextRecipe = after->nextRecipe;
  after->nextRecipe = newRep;
  return newRep;
}

Recipe_t repAddAfter(Recipe_t head, Recipe_t newRep)
{
  Recipe_t current = head;
  Recipe_t addAfter = NULL;
  while(current->nextRecipe != NULL)
  {
    if(strcmp(current->description, newRep->description) < 0) 
    {
      addAfter = current;  
    }
    current = current->nextRecipe;
  }
  
  if(strcmp(current->description, newRep->description) < 0) 
  {
    addAfter = current;  
  }
  
  return addAfter;
}

Recipe_t existRecipe(Recipe_t head, Recipe_t newRep)
{
  Recipe_t current = head;

  while(current->nextRecipe != NULL)
  {
    if(strcmp(current->description, newRep->description) == 0)
    {
      return current;
    }
    current = current->nextRecipe;
  }

  if(strcmp(current->description, newRep->description) == 0)
  {
    return current;
  }
  return NULL;
}

void recipeToString(Recipe_t head)
{
  Recipe_t current = head;
  int cnt = 1;
  while(current->nextRecipe != NULL)
  {
    printf("   %d - %s\n", cnt++, current->description);
    current = current->nextRecipe;
  }

  printf("   %d - %s\n", cnt++, current->description);
}

void removeRecipe(Recipe_t recipe)
{
  //FREE recipe first
  free(recipe);
}
