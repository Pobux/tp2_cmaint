#ifndef CAT_H
#define CAT_H
#include "recipe.h"
typedef struct CategoryIntern_t
{
  char description[120]; 
  struct CategoryIntern_t* nextCategory;
  struct RecipeIntern_t* recipeHead;
} CategoryIntern_t;

typedef CategoryIntern_t* Category_t;

/**
 * Create new category
 */
Category_t newCategory(char* description, char* recipe);

/**
 * Evaluate head
 */
Category_t evaluateHead(Category_t head, Category_t newCat);

/**
 * 3 options 
 * New category chain
 * New category is to be the new head 
 * The new category is to be inserted somewhere in the chained list 
 */
Category_t addCategory(Category_t head, Category_t newCat);

/**
 * Loop in chained list and add the category at the right spot
 */
void addCategoryInChain(Category_t head, Category_t newCat);

/**
 * 3 options
 * Category has no recipe
 * New recipe is to be the new head
 * The new recipe is to be inserted somewhere in the chained list (@addInChain)
 */
void addRecipe(Category_t cat, Recipe_t);

/**
 * Loop in chained list and add the recipe at the right spot 
 */
void addRecipeInChain(Recipe_t current, Recipe_t recipe);

/**
 * Define after which position new category has to be inserted 
 */
Category_t addAfter(Category_t head, Category_t newCat);

/**
 * Do the actual insertion by swapping adresses
 */
Category_t doInsert(Category_t after, Category_t newCat);

/**
 * Check if a category exist
 */
Category_t existCategory(Category_t head, Category_t newCat);

/**
 * Print each member of the array list
 */
void categoryToString(Category_t head);

/**
 * Compare string alphanumericly
 */
int compareDescription(char* string1, char* string2);

/**
 * Free memory
 */
void removeCategory(Category_t category);

#endif
