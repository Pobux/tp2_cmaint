#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "category.c"
#include "recipe.c"

int main(int argc, char *argv[])
{
  Category_t head = NULL;

  Category_t cat2 = newCategory("ZZZ", "lol");
  Category_t cat3 = newCategory("BAA", "lol");
  Category_t cat4 = newCategory("CAA", "lol");
  Category_t cat5 = newCategory("ACA", "lol");
  Category_t cat6 = newCategory("ACA", "lol");
  Category_t cat7 = newCategory("LOL", "lol");
  Category_t cat8 = newCategory("ACAA", "lol");
  Category_t cat9 = newCategory("Aaa", "lol");
  
  head = addCategory(head, cat2);
  head = addCategory(head, cat3);
  head = addCategory(head, cat4);
  head = addCategory(head, cat5);
  head = addCategory(head, cat6);
  head = addCategory(head, cat7);
  head = addCategory(head, cat8);
  head = addCategory(head, cat9);
  
  categoryToString(head);
  return 0;
}
