#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include "parse.h"

void parseRecipe(char* buffer, int catIndex, char* recipe)
{
  int startIdx = 0; 
  int recipeCnt = 0;

  while(buffer[startIdx] == ' ')
  {
    startIdx++;
  }
  
  if(buffer[startIdx] == '[')
  {
    recipe = NULL;
    return;
  }

  while(startIdx != catIndex) 
  {
    recipe[recipeCnt] = tolower(buffer[startIdx]); 
    recipeCnt++;
    startIdx++;
  }
}

int parseCategory(char* buffer, int catIndex, char* categoryName)
{
  int startIdx = catIndex + 2; //after _[
  int categoryCnt = 0;

  while(buffer[startIdx] != ']')
  {
    categoryName[categoryCnt] = tolower(buffer[startIdx]);
    startIdx++;
    categoryCnt++;
  }

  return startIdx + 1; // pass next [
}

int indexOfCategory(char* buffer, char* lookFor)
{
  char* found;
  
  found = strstr(buffer, lookFor);
  if(buffer[0] == '[')
  {
    printf("Vous ne pouvez pas ajouter une catégorie sans recette, ligne invalide dans le fichier\n");
    exit(1);
  }
  else if(found != NULL)
  {
    if(strcmp(lookFor, "]") == 0)
    {
      return found - buffer;   
    }
    return trimmedIdx(buffer, found - buffer);   
  }
  else
  {
    return -1;
  }
}

int trimmedIdx(char* buffer, int limit)
{
  while(buffer[limit] != ' ')
  {
    limit--;
  }
  return limit--;
}
