CC=gcc
CFLAG=-Wall
OBJ = utils.o reader.o recipe.o category.o parse.o search.o main.o
EXE = tp2

.SUFFIXES: .c .o

all: link

compile: $(OBJ)

.c.o:
	$(CC) $(CFLAG) -c $*.c

link: compile
	$(CC) $(OBJ) -o $(EXE)
	
clean:
	rm $(OBJ) 

