#ifndef PARSE_H
#define PARSE_H

/**
 * Find index of category part in buffer
 */
int indexOfCategory(char* buffer, char* lookFor);

/**
 * Get recipe name
 * Does trim check on recipe
 */
void parseRecipe(char* buffer, int catIndex, char* recipe);

/**
 * Retrieve category name
 * Manage begin [ and end ]
 */
int parseCategory(char* buffer, int catIndex, char* categoryName);

/**
 * Trim end of recipe string
 * Return index at which recipe string ends
 */
int trimmedIdx(char* buffer, int limit);

#endif
