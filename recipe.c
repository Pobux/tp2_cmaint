#include <assert.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "recipe.h"
#include "utils.h"

Recipe_t newRecipe(char* description)
{
  Recipe_t recipe;
  recipe = malloc(sizeof(RecipeIntern_t));
  
  if(description != NULL)
  {
    strcpy(recipe->description, description);
  }

  return recipe;
}

void recipeToString(Recipe_t head)
{
  Recipe_t current = head;

  while(current->nextRecipe != NULL)
  {
    printClean(current->description);
    current = current->nextRecipe;
  }

  printClean(current->description);
}

void removeRecipe(Recipe_t recipe)
{
  //FREE recipe first
  Recipe_t tmp;
  while(recipe->nextRecipe != NULL)
  {
    tmp = recipe->nextRecipe;
    free(recipe);
    recipe = tmp;
  }
  free(recipe);
}
