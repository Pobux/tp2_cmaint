#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "category.h"
#include "main.h"
#include "reader.h"
#include "parse.h"
#include "recipe.h"
#include "search.h"
 
const int lineMaxSize = 120;

int main(int argc, char *argv[])
{
  FILE* recipeFile;
  Category_t categoryHead;

  validateMainInput(argc);
  recipeFile = openFile(argv[1], "r"); 
  categoryHead = buildStruct(recipeFile);

  if(categoryHead == NULL)
  {
    printf("Catégorie vide, rien à rechercher\n");
    exit(1);
  }

  launchSearch(categoryHead);
  removeCategory(categoryHead); //free
  closeFile(recipeFile);
  return 0;
}

void validateMainInput(int argc)
{
  if(argc != 2)
  {
    printf("Paramètres d'entrée incorrects\n");
    exit(1);
  }
}

Category_t buildStruct(FILE* recipeFile)
{
  char buffer[lineMaxSize + 1];
  char recipe[lineMaxSize];
  int categoryIdx = -1;
  Category_t head = NULL;

  //For each line, find category and recipe, reset names at each loop
  while(fgets(buffer, lineMaxSize, recipeFile) != NULL)
  {
    memset(recipe, 0, lineMaxSize);
    categoryIdx = indexOfCategory(buffer, "[");
    if(categoryIdx == -1)
    {
      printf("Ligne du fichier mal formattée. Aucun [categorie].\n:: %s", buffer);
      exit(1);
    }
    
    //recipe is updated here at each loop 
    parseRecipe(buffer, categoryIdx, recipe); 
    head = manageCategory(buffer, categoryIdx, head, recipe);
  }
  return head;
}

Category_t manageCategory(char* buffer, int categoryIdx, Category_t head, char* recipe)
{
  Category_t currentCategory;
  char categoryName[lineMaxSize];

  //Match string len after last ] with + 2
  //until buffer is empty, reset category name at each loop
  while(categoryIdx + 2 < strlen(buffer))  
  {
    memset(categoryName, 0, lineMaxSize);
    
    //Find category in buffer, return idx for the next one
    categoryIdx = parseCategory(buffer, categoryIdx, categoryName); 

    //Standard format 
    categoryName[0] = toupper(categoryName[0]); 
    currentCategory = newCategory(categoryName, recipe); 

    head = addCategory(head, currentCategory);
  }

  return head;
}
