#include <ctype.h>
#include <stdio.h>

void printClean(char* str)
{
  int idx = 0;
  while(str[idx] != '\0')
  {
    str[idx] = tolower(str[idx]);
    idx++;
  }
  str[0] = toupper(str[0]);
  printf("%s\n", str);
  
  //Set back string as it was
  str[0] = tolower(str[0]);
}
