#ifndef READER_H
#define READER_H
/**
 * close file while managing error
 */
void closeFile(FILE* file);

/**
 * open file while managing error
 */
FILE* openFile(char* filename, char* type);
#endif
