#ifndef MAIN_H
#define MAIN_H

/**
 * Validate main arguments input
 * There should be only one
 */
void validateMainInput(int argc);

/**
 * Building structure for later search
 * @uses parser to dispatchRecipes
 */
Category_t buildStruct(FILE* recipeFile);

/**
 * Create category for each line 
 */
Category_t manageCategory(char* buffer, int categoryIdx, Category_t head, char* recipe); 

#endif
