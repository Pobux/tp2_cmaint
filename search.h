#ifndef SEARCH_H
#define SEARCH_H
#include "category.h"

/**
 * Initiate launch process
 */
void launchSearch(Category_t categoryHead);

/**
 * Ask for search input
 */
char* promptSearch();

/**
 * Ensure validation and pass correct input to search process
 */
void search(Category_t head, char* searchInput);

/**
 * Dynamicly allocate search input
 */
char* getStdin();

/**
 * Look for category part in search input
 * There's no category creation, only string checks
 */
char* findCategory(char* searchInput);

/**
 * Validate and pass to LookForRecipe() part in search input
 * There's no recipe creation, only string checks
 */
char* findRecipe(char* searchInput);

/**
 * Validate search input by trimming and counting words
 */
int validateSearchInput(char* searchInput);

/**
 * From given category, match correct recipe
 */
void lookForRecipe(Category_t catStruct, char* category, char* recipe);

/**
 * Compare with category's recipes description
 * If the new recipe match, success if it does
 */
void recipeSearchResult(Category_t head, char* category, char* recipe);

/**
 * Goes through category chained list and match category
 */

Category_t categorySearchResult(Category_t head, char* category);

/**
 * There's two type : Category, Category and recipe
 * Search and print correct result
 */
void dispatchSearchByType(Category_t head, char* searchInput, int searchType);

/**
 * 3 states
 * -1, input is wrong
 *  0, there's only a category
 *  1, there's a category and a recipe
 */
int checkSearchInput(char* searchInput);

/**
 * Remove whitespace before and after string
 */
char* trim(char* str);

#endif
