Antoine LeBel

LEBA23068603

INF3135 - Conception et maintenance de logiciel

Professeur : Jacques Berger

Recherche recette
=================
Le programme prend un fichier de recette et de catégorie en paramètre et
construit un ensemble dans lequel il est possible de chercher les catégories et les recettes.

Compilation
===========
Tapper `make` en ligne de commande pour compiler le site.

Le programme contient :

* main.c
* parse.c : gère les lignes du fichier et recherche la recette et les catégorie 
* reader.c : lecture et fermeture du fichier
* recipe.c
* category.c
* search.c : gére la partie recherche du programme
* utils.c

Exécution
=========
Le fichier créé se nomme tp2

Pour l'exécuter vous devez tapper `./tp2 [le nom du fichier]`


