#ifndef UTILS_H
#define UTILS_H

/**
 * Format string before printing
 * First letter to uppercase, the rest in lower case.
 */
void printClean(char* str);

#endif
